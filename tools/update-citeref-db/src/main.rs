use std::error::Error;
use std::ffi::OsString;

use mysql::{Conn, Opts, OptsBuilder, Params, Statement, Value};
use mysql::prelude::*;

use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use flate2::read::GzDecoder;
use tar::Archive;
use serde::Deserialize;
use toolforge;
use std::fs;

#[derive(Deserialize)]
struct ArticleBody {
    // wikitext: String,
    html: Option<String>
}


#[derive(Deserialize)]
struct EnterprisePage {
    name: String,
    // identifier: u32,
    // date_modified: String,
    article_body: Option<ArticleBody>
}

const ROWS_PER_INSERT: usize = 5000;

fn prepare_insert_statement(conn: &mut Conn, row_count: usize) -> Statement{
    const BASE_QUERY: &'static str = "INSERT INTO citerefs (page_title, citeref_text) VALUES (?, ?)";
    const ADDITIONAL_PIECE: &'static str = ", (?, ?)";
    let mut query = String::with_capacity(BASE_QUERY.len() + (row_count - 1) * ADDITIONAL_PIECE.len());
    query.push_str(BASE_QUERY);
    for _ in 0..(row_count - 1) {
        query.push_str(ADDITIONAL_PIECE);
    }
    query.as_statement(conn).expect("Failure should be impossible here").into_owned()
}

fn write_to_db(conn: &mut Conn, insert_statement: &Statement, rows: &Vec<(String, String)>) -> Result<(), mysql::Error> {
    let values: Vec<Value> = rows.iter().map(|(title, id)| [title, id]).flatten().map(Value::from).collect();
    conn.exec_drop(insert_statement, Params::Positional(values))
}

fn connect_to_db() -> Result<Conn, Box<dyn Error>> {
    let db_connection_info = toolforge::connection_info!("enwiki")?;
    let opts: Opts = OptsBuilder::new()
        .user(Some(&db_connection_info.user))
        .pass(Some(&db_connection_info.password))
        .ip_or_hostname(Some("tools.db.svc.wikimedia.cloud"))
        .db_name(Some(db_connection_info.user + "__citation_toolkit")).into();
    let conn = Conn::new(opts)?;
    Ok(conn)
}

fn begin_db_update(conn: &mut Conn) -> Result<(), mysql::Error> {
    conn.query_drop("UPDATE status_subtool SET subtool_status = 0 WHERE subtool_name = 'lookup-citerefs'")?;
    conn.query_drop("TRUNCATE TABLE citerefs")
}

fn end_db_update(conn: &mut Conn) -> Result<(), mysql::Error> {
    conn.query_drop("UPDATE status_subtool SET subtool_status = 1 WHERE subtool_name = 'lookup-citerefs'")
}

fn get_ids_string_matching(document: &str) -> Vec<&str> {
    let mut ids: Vec<&str> = Vec::new();
    let mut pointer: usize = 0;

    const PAT_BEGIN: &str = "id=\"CITEREF";
    const PAT_END: char = '"';

    loop {
        if let Some(start_offset) = document[pointer..].find(PAT_BEGIN) {
            let start_index = pointer + start_offset + PAT_BEGIN.len();
            if let Some(captured_length) = document[start_index..].find(PAT_END) {
                let end_index = start_index + captured_length;
                pointer = end_index + 1;
                let citeref = &document[start_index..end_index];
                ids.push(citeref)
            }
        } else {
            break
        }
    }
    ids
}

fn trim_below(citeref: &str, length: usize) -> &str {
    // floor_char_boundary would be great for this if it was stable
    if citeref.len() <= length {
        return citeref
    } else if citeref.is_char_boundary(length) {
        return &citeref[0..length]
    } else {
        let mut boundary = length;
        while boundary > 0 {
            boundary = boundary - 1;
            if citeref.is_char_boundary(boundary) {
                return &citeref[0..boundary]
            }
        }
        return ""
    }
}

#[test]
fn check_correct_trim() {
    assert_eq!(trim_below("☃☃☃", 0), "");
    assert_eq!(trim_below("☃☃☃", 1), "");
    assert_eq!(trim_below("☃☃☃", 2), "");
    assert_eq!(trim_below("☃☃☃", 3), "☃");
    assert_eq!(trim_below("☃☃☃", 4), "☃");
    assert_eq!(trim_below("☃☃☃", 5), "☃");
    assert_eq!(trim_below("☃☃☃", 6), "☃☃");
    assert_eq!(trim_below("☃☃☃", 7), "☃☃");
    assert_eq!(trim_below("☃☃☃", 8), "☃☃");
    assert_eq!(trim_below("☃☃☃", 9), "☃☃☃");
    assert_eq!(trim_below("☃☃☃", 10), "☃☃☃");
}


fn main() -> Result<(), Box<dyn Error>> {
    let mut conn = connect_to_db()?;
    begin_db_update(&mut conn)?;
    let dumps_directory = "/public/dumps/public/other/enterprise_html/runs";
    let mut directory_entries = fs::read_dir(dumps_directory)?.collect::<Result<Vec<fs::DirEntry>, _>>()?;
    directory_entries.sort_by(|a, b| b.file_name().cmp(&a.file_name()));
    let newest_dump = directory_entries.first().expect("There should always be a dump file");
    let mut filename = OsString::new();
    filename.push("enwiki-NS0-");
    filename.push(newest_dump.file_name());
    filename.push("-ENTERPRISE-HTML.json.tar.gz");
    let filepath = newest_dump.path().join(filename);
    let file = File::open(filepath)?;
    let gz = GzDecoder::new(file);
    let mut tar = Archive::new(gz);
    let mut rows: Vec<(String, String)> = Vec::with_capacity(ROWS_PER_INSERT);
    let mut insert_statement = prepare_insert_statement(&mut conn, ROWS_PER_INSERT);
    for maybe_entry in tar.entries()? {
        let entry = maybe_entry?;
        let buf_entry = BufReader::new(entry);
        for maybe_line in buf_entry.lines() {
            let line = maybe_line?;
            let deserialized: EnterprisePage = serde_json::from_str(&line)?;
            if let Some(html) = deserialized.article_body.map(|x| x.html).flatten() {
                let ids = get_ids_string_matching(&html);
                for id in ids {
                    rows.push((deserialized.name.to_owned(), trim_below(id, 767).to_owned()));
                    if rows.len() == ROWS_PER_INSERT {
                        match write_to_db(&mut conn, &insert_statement, &rows) {
                            Err(mysql::Error::MySqlError(mysql::MySqlError { code: 1615, .. })) => {
                                // This is the "prepared statement needs to be re-prepared" code. Thus, re-prepare
                                insert_statement = prepare_insert_statement(&mut conn, ROWS_PER_INSERT);
                                write_to_db(&mut conn, &insert_statement, &rows)?;
                            },
                            Err(err) => {
                                return Err(err.into());
                            }
                            Ok(_) => {}
                        }
                        rows.clear();
                    }
                }
            }
        }
    }
    if !rows.is_empty() {
        insert_statement = prepare_insert_statement(&mut conn, rows.len());
        write_to_db(&mut conn, &insert_statement, &rows)?;
    }
    end_db_update(&mut conn)?;
    return Ok(())
}
