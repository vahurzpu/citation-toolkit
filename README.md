# Citation Toolkit

This is a collection of web-based tools for working with citations on Wikipedia. Right now, this focuses solely on English Wikipedia; please leave a note at [User talk:Vahurzpu](https://en.wikipedia.org/wiki/User_talk:Vahurzpu) if you're interested in adapting it to your wiki.

## Subtools

- [Find pages with a particular CITEREF anchor](https://citation-toolkit.toolforge.org/lookup-citeref)
- [Track pages with Harv and Sfn no-target errors](https://citation-toolkit.toolforge.org/track-no-target)
- [Highlight which text in an article is supported by which citations](https://citation-toolkit.toolforge.org/highlight-citations)