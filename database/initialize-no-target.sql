CREATE TABLE no_target(
    page_title varbinary(255),
    missing_anchor varchar(1024),
    genuine_error integer,
    last_updated integer
);

CREATE INDEX no_target_title_ix ON no_target(page_title);