CREATE TABLE status_subtool (
    subtool_name varchar(50),
    subtool_status tinyint(1),
    subtool_last_update bigint
);

INSERT INTO status_subtool (subtool_name, subtool_status, subtool_last_update) VALUES ('lookup-citerefs', 0, 0);