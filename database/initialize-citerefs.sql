CREATE TABLE citerefs (
    page_title varchar(255),
    citeref_text varbinary(767)
);

CREATE INDEX citeref_ix ON citerefs(citeref_text);
