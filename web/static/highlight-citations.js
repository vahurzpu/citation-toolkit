document.addEventListener('DOMContentLoaded', function () {
    const usageStats = JSON.parse(document.getElementById('usage-stats').textContent)
    citationKeys = Array.from(Object.keys(usageStats))
    citationKeys.sort((a, b) => {
        partsA = a.split('-')
        partsB = b.split('-')   
        key1 = Number(partsA[partsA.length - 1])
        key2 = Number(partsB[partsB.length - 1])
        return key1 - key2
    })
    citationColors = {}
    for (let i = 0; i < citationKeys.length; i++) {
        citationColors[citationKeys[i]] = d3.interpolateSpectral(Math.random())
    }

    function refreshColors() {
        document.querySelectorAll('[data-citation]').forEach(elem => {
            elem.style.backgroundColor = citationColors[JSON.parse(elem.dataset.citation)[0]['id']]
        })    
    }

    d3
        .select('aside')
        .append('ol')
        .selectAll('li')
        .data(citationKeys)
        .join('li')
        .html(d => {
            if (d == "<uncited>") {
                return "&lt;uncited&gt;"
            }
            referent = document.getElementById(d)
            if (!referent) {
                return d
            }
            referent = referent.cloneNode(true)
            if (d.startsWith("CITEREF")) {
                referent.removeAttribute('id')
                return referent.outerHTML
            } else if (d.startsWith("cite_note")) {
                refContents = referent.querySelector('.mw-reference-text')
                refContents.removeAttribute('id')
                return refContents.outerHTML
            } else {
                alert('weird reference')
            }
        })
        .style('background-color', d => citationColors[d])
        .on('mouseover', (e, d) => {
            document.querySelectorAll('[data-citation]').forEach(elem => {
                if (!JSON.parse(elem.dataset.citation).map(x => x.id).includes(d)) {
                    elem.style.backgroundColor = null
                } else {
                    elem.style.backgroundColor = citationColors[d]
                }
            })        
        })
        .on('mouseout', refreshColors)
        .on('click', (e, d) => {
            for (let elem of document.querySelectorAll('[data-citation]')) {
                if (JSON.parse(elem.dataset.citation).map(x => x.id).includes(d)) {
                    elem.scrollIntoView({behavior: 'smooth'})
                }
            }
        })
    refreshColors()
})