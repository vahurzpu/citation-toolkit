function isYear(arg) {
    const regexes = [
        /^\d{3}\d?[a-z]?$/,
        /^n\.d\.[a-z]?$/,
        /^nd[a-z]?$/,
        /^c\. \d{3}\d?[a-z]?$/,
        /^\d{4}–\d{4}[a-z]?$/,
        /^\d{4}–\d{2}[a-z]?$/,
    ]
    return regexes.some(regex => arg.match(regex))
}

function sfnref(args) {
    // Based on the function of the same name in Module:Footnotes
    let out = []
    if (args.length <= 5) {
        out = args
    } else {
        out = args.slice(0, 5)
        for (let i = 5; i < args.length; i++) {
            if (isYear(args[i])) {
                out.push(args[i])
                break
            }
        }
    }
    return out.join('')
}

function resolveRef(text) {
    if (text.startsWith('{')) {
        if (text.startsWith('{{') && text.endsWith('}}')) {
            return sfnref(text.slice(2, -2).split('|').slice(1))
        }
    } else if (text.startsWith('CITEREF')) {
        return text.slice(7)
    } else if (!text.includes(' ')) {
        return text
    }
}

function updateResultList(searchInput) {
    const ref = resolveRef(searchInput)
    if (ref) {
        const params = (new URLSearchParams({ref})).toString()
        fetch('/lookup-citeref/matching?' + params)
            .then(r => r.json())
            .then(r => {
                results = document.getElementById('result-list')
                results.replaceChildren()
                for (const pageName of r) {
                    let li = document.createElement('li')
                    let a = document.createElement('a')
                    a.href = 'https://en.wikipedia.org/wiki/' + pageName.replace(/ /g, '_') + '#CITEREF' + ref
                    a.textContent = pageName
                    li.appendChild(a)
                    results.appendChild(li)
                }
            })
    }
}

window.addEventListener('DOMContentLoaded', () => {
    const refBox = document.getElementById('ref')
    refBox.addEventListener('input', e => {
        updateResultList(refBox.value)
    })

    const params = new URLSearchParams(document.location.search)
    if (params.has('ref')) {
        refBox.value = params.get('ref')
        updateResultList(params.get('ref'))
    }
})