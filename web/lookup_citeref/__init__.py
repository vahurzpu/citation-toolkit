from flask import Blueprint, jsonify, render_template, request
from .. import db

bp = Blueprint('lookup-citeref', __name__, url_prefix='/lookup-citeref')

@bp.route('/')
def main():
    conn = db.get_db()
    with conn.cursor() as cur:
        cur.execute("SELECT subtool_status FROM status_subtool WHERE subtool_name = 'lookup-citerefs'")
        currently_updating = not bool(cur.fetchone()[0])
    return render_template('lookup-citeref.html', currently_updating=currently_updating)

@bp.route('/matching')
def api():
    ref = request.args['ref']
    conn = db.get_db()
    with conn.cursor() as cur:
        cur.execute('select distinct page_title from citerefs where citeref_text = %s', (ref,))
        matching_titles = [x[0] for x in cur.fetchall()]
        return jsonify(matching_titles)
