import requests
from bs4 import BeautifulSoup

def fetch_html(article_name: str) -> BeautifulSoup:
    article_text = requests.get('https://en.wikipedia.org/api/rest_v1/page/html/' + article_name.replace(' ', '_')).text
    return BeautifulSoup(article_text, 'lxml')
