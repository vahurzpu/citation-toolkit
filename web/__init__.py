from flask import Flask, render_template
from . import lookup_citeref, track_no_target, highlight_citations
from .db import init_app as init_db

def create_app():
    app = Flask(__name__, instance_relative_config=True)
    app.register_blueprint(lookup_citeref.bp)
    app.register_blueprint(track_no_target.bp)
    app.register_blueprint(highlight_citations.bp)

    @app.route("/")
    def main_page():
        return render_template("main.html")

    init_db(app)
    return app
