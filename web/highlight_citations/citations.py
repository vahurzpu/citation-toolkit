from typing import Any, Dict
from .nodes import Node, SfnNode, RpNode, RefNode

class Citation(object):
    def __init__(self) -> None:
        pass
    def as_dict(self) -> Dict[str, Any]:
        raise NotImplemented

class SfnCitation(Citation):
    def __init__(self, node: SfnNode) -> None:
        self.id = node.id
        self.param_name = node.param_name
        self.param_value = node.param_value
        super().__init__()
    def as_dict(self) -> Dict[str, Any]:
        obj = {'type': 'sfn', 'id': self.id}
        obj[self.param_name] = self.param_value
        return obj

class RpCitation(Citation):
    def __init__(self, rp_node: RpNode, ref_node: RefNode) -> None:
        self.id = ref_node.id
        self.param_name = rp_node.param_name
        self.param_value = rp_node.param_value
        super().__init__()
    def as_dict(self) -> Dict[str, Any]:
        obj = {'type': 'rp', 'id': self.id}
        obj[self.param_name] = self.param_value
        return obj

class RefCitation(Citation):
    def __init__(self, node: RefNode) -> None:
        self.id = node.id
        super().__init__()
    def as_dict(self) -> Dict[str, Any]:
        obj = {'type': 'ref', 'id': self.id}
        return obj
