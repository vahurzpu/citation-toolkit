from collections import defaultdict
import json
import copy
from flask import render_template, request, redirect
from bs4 import BeautifulSoup
from bs4.element import Tag
from typing import List, Dict
from .nodes import Node, SfnNode, RpNode, RefNode, CitationNeededNode, GenericNode
from .citations import Citation, SfnCitation, RpCitation, RefCitation
from ..utils import fetch_html
from flask import Blueprint

bp = Blueprint('highlight-citations', __name__, url_prefix='/highlight-citations')

def get_support(para: Tag) -> List:
    supports = []
    current_citations: List[Citation] = []
    citations_used = False
    i = len(para.contents) - 1
    while i >= 0:
        wrapped_node = Node.create(para.contents[i])
        if isinstance(wrapped_node, GenericNode):
            supports.append([cite.as_dict() for cite in current_citations])
            citations_used = True
            i -= 1
            continue

        # If we've made it this far, we're dealing with some sort of citation
        if citations_used:
            current_citations = []
        citations_used = False

        if isinstance(wrapped_node, RpNode):
            previous_node = Node.create(para.contents[i - 1])
            assert isinstance(previous_node, RefNode)
            current_citations.append(RpCitation(wrapped_node, previous_node))
            supports.append([RpNode.marker()])
            supports.append([RefNode.marker()])
            i -= 2
        elif isinstance(wrapped_node, SfnNode):
            current_citations.append(SfnCitation(wrapped_node))
            supports.append([SfnNode.marker()])
            i -= 1
        elif isinstance(wrapped_node, RefNode):
            current_citations.append(RefCitation(wrapped_node))
            supports.append([RefNode.marker()])
            i -= 1
        elif isinstance(wrapped_node, CitationNeededNode):
            current_citations = []
            supports = []
            i -= 1

    supports.reverse()
    assert len(supports) == len(para.contents)
    return supports

def usage_statistics(body_paragraphs, supports) -> Dict[str, float]:
    usage = defaultdict(float)
    for (para, para_support) in zip(body_paragraphs, supports, strict=True):
        for (node, node_support) in zip(para.contents, para_support, strict=True):
            text_length = len(node.get_text())
            if len(node_support) == 0:
                usage['<uncited>'] += text_length
            for citation in node_support:
                if not citation['type'].startswith('marker-'):
                    usage[citation['id']] += text_length / len(node_support)
    return dict(usage)

def highlight_paragraph(article: BeautifulSoup, para: Tag, support):
    original_nodes = para.contents
    para.contents = []
    current_citation = support[0]
    current_collection = []

    def flush():
        nonlocal current_collection
        nonlocal current_citation
        if len(current_collection) > 0:
            new_span = article.new_tag('span', attrs={'data-citation': json.dumps(current_citation)})
            new_span.extend(current_collection)
            para.append(new_span)
        current_citation = None
        current_collection = []

    for (supporting, node) in zip(support, original_nodes, strict=True):
        if len(supporting) == 0 or supporting[0]['type'] in {'marker-rp', 'marker-ref', 'marker-sfn'}:
            flush()
            para.append(copy.copy(node))
        elif supporting == current_citation:
            current_collection.append(copy.copy(node))
        else:
            flush()
            current_citation = supporting
            current_collection = [copy.copy(node)]
    flush()

def get_body_paragraphs(article):
    sections: List[Tag] = article.body.find_all('section', recursive=False)
    [lead_section, *body_sections] = sections
    body_paragraphs: List[Tag] = []
    for section in body_sections:
        body_paragraphs.extend(section.find_all('p', recursive=True))
    return body_paragraphs

@bp.route("/<site>/<title>")
def highlight_citations(site: str, title: str):
    assert site == 'en.wikipedia.org', 'non-enwiki sites not yet supported'
    article = fetch_html(title)
    body_paragraphs = get_body_paragraphs(article)
    supports = [get_support(para) for para in body_paragraphs]
    usage_stats = usage_statistics(body_paragraphs, supports)
    for (support, para) in zip(supports, body_paragraphs):
        highlight_paragraph(article, para, support)

    
    contents = article.new_tag('main')
    contents.attrs['class'] = article.body.attrs['class']
    contents.extend(article.body.contents)

    return render_template(
        'highlight-citations-page.html',
        headparts=''.join(map(str, article.head.contents)),
        usagestats=json.dumps(usage_stats),
        bodyparts=str(contents)
    )

@bp.route('/', methods=['GET', 'POST'])
def main_page():
    if request.method == 'GET':
        return render_template('highlight-citations-form.html')
    else:
        site = request.form['site']
        page = request.form['page'].replace(' ', '_')
        return redirect(f'/highlight-citations/{site}/{page}')