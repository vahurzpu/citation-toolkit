import json
from bs4 import PageElement
from bs4.element import Tag
from typing import Dict

def has_class(node: PageElement, class_: str) -> bool:
    return type(node) is Tag and 'class' in node.attrs and class_ in node.attrs['class']

def has_typeof(node: PageElement, typeof: str) -> bool:
    return type(node) is Tag and 'typeof' in node.attrs and typeof in node.attrs['typeof'].split()

class Node(object):
    def __init__(self, node: PageElement) -> None:
        self.node = node
    def create(node: PageElement) -> "Node":
        if SfnNode.recognize(node):
            return SfnNode(node)
        if RefNode.recognize(node):
            return RefNode(node)
        if RpNode.recognize(node):
            return RpNode(node)
        if CitationNeededNode.recognize(node):
            return CitationNeededNode(node)
        if GenericNode.recognize(node):
            return GenericNode(node)
    def __repr__(self) -> str:
        return f"{type(self).__name__}({self.node})"
    def marker() -> Dict[str, str]:
        raise NotImplemented

class SfnNode(Node):
    def __init__(self, node: PageElement) -> None:
        params = json.loads(node.attrs['data-mw'])['parts'][0]['template']['params']
        aliases = {
            'page': 'page',
            'p': 'page',
            'pages': 'pages',
            'pp': 'pages',
            'loc': 'location'
        }
        self.param_name = None
        self.param_value = None
        for (name, value) in params.items():
            if name in aliases:
                self.param_name = aliases[name]
                self.param_value = value['wt']
        top_level = node
        while top_level.parent != None:
            top_level = top_level.parent            
        self.footnote_id = node.a.attrs['href'].split('#')[1]
        footnote = top_level.find(id=self.footnote_id)
        ref_contents = footnote.find(class_='mw-reference-text')
        self.id = ref_contents.a.attrs['href'].split('#')[1]
        super().__init__(node)
    def recognize(node: PageElement) -> bool:
        if has_class(node, 'mw-ref') and has_typeof(node, 'mw:Transclusion'):
            data_mw = json.loads(node.attrs['data-mw'])
            assert 'parts' in data_mw and len(data_mw['parts']) == 1
            template_ref = data_mw['parts'][0]['template']['target']['href'].split(':')[1]
            return template_ref in {'Sfn'}
        return False
    def marker() -> Dict[str, str]:
        return {"type": "marker-sfn"}

class RpNode(Node):
    def __init__(self, node: PageElement) -> None:
        params = json.loads(node.attrs['data-mw'])['parts'][0]['template']['params']
        aliases = {
            'page': 'page',
            'p': 'page',
            'pages': 'pages',
            'pp': 'pages',
            '1': 'pages',
            'location': 'location',
            'loc': 'location',
            'at': 'location'
        }

        for (name, value) in params.items():
            if name in aliases:
                self.param_name = aliases[name]
                self.param_value = value['wt']

        super().__init__(node)
    def recognize(node: PageElement) -> bool:
        if has_class(node, 'reference') and has_typeof(node, 'mw:Transclusion'):
            data_mw = json.loads(node.attrs['data-mw'])
            assert 'parts' in data_mw and len(data_mw['parts']) == 1
            template_ref = data_mw['parts'][0]['template']['target']['href'].split(':')[1]
            return template_ref in {'Rp'}
        return False
    def marker() -> Dict[str, str]:
        return {"type": "marker-rp"}

class RefNode(Node):
    def __init__(self, node: PageElement) -> None:
        self.id = node.a.attrs['href'].split('#')[1]
        super().__init__(node)
    def recognize(node: PageElement) -> bool:
        return has_class(node, 'mw-ref')
    def marker() -> Dict[str, str]:
        return {"type": "marker-ref"}

class CitationNeededNode(Node):
    def __init__(self, node: PageElement) -> None:
        super().__init__(node)
    def recognize(node: PageElement) -> bool:
        return has_class(node, 'Template-Fact')
    def marker() -> Dict[str, str]:
        return {"type": "marker-cn"}

class GenericNode(Node):
    def __init__(self, node: PageElement) -> None:
        super().__init__(node)
    def recognize(node: PageElement) -> bool:
        return True