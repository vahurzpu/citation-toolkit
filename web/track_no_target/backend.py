from typing import Tuple, List, Set, Literal
import requests
from bs4 import BeautifulSoup
import time
import mwapi
import logging

user_agent = 'Harv and Sfn no-target tracker/0.0.1 (User:Vahurzpu) ' + requests.utils.default_user_agent()
template_names = {'harvnb', 'sfn', 'sfnp', 'harvtxt', 'harv', 'harvp', 'sfnm', 'harvcol', 'harvcoltxt', 'harvcolnb', 'sfnmp', 'harvc'}

def fetch_page(page_title: str) -> Tuple[str, BeautifulSoup]:
    api_url = 'https://en.wikipedia.org/api/rest_v1/page/html/' + page_title.replace('/', '%2F')
    res = requests.get(api_url, headers={'User-Agent': user_agent})
    if res.ok:
        return ('ok', BeautifulSoup(res.text, 'lxml'))
    elif res.status_code == 404:
        return ('not found', None)
    else:
        return ('failed', None)

def categorize_errors(page_html: BeautifulSoup) -> List[Tuple[str, bool]]:
    flagged_errors = []
    for problematic_harv in page_html.find_all(class_="harv-error"):
        just_citeref: str = problematic_harv.get_text()
        for tl_name in template_names:
            just_citeref = just_citeref.removeprefix(f' {tl_name} error: no target: ')
        just_citeref = just_citeref.removeprefix('Harvc error: no target: ') # uniquely, doesn't have a space, and is capitalized
        just_citeref = just_citeref.removesuffix(' (help)')
        matching_elem = page_html.find(id=just_citeref)
        flagged_errors.append((just_citeref, not matching_elem))
    return flagged_errors

def get_pages_in_category() -> Set[str]:
    session = mwapi.Session('https://en.wikipedia.org/', user_agent=user_agent)
    continued = session.get(
        formatversion=2,
        action='query',
        generator='categorymembers',
        gcmtitle='Category:Harv and Sfn no-target errors',
        gcmlimit=100,
        continuation=True)
    pages = set()
    for portion in continued:
        if 'query' in portion:
            for page in portion['query']['pages']:
                pages.add(page['title'].replace(' ', '_'))
    return pages

def get_pages_in_database(conn) -> Set[str]:
    try:
        cur = conn.cursor()
        cur.execute('SELECT DISTINCT page_title FROM no_target')
        return {s if isinstance(s, str) else s.decode('utf8') for (s,) in cur.fetchall()}
    finally:
        cur.close()

class Backend:
    def __init__(self, conn) -> None:
        self.conn = conn

    def refresh_page(self, page_title: str):
        now_ts = int(time.time())
        status, fetched_page = fetch_page(page_title)
        if status == 'ok':
            flagged_errors = categorize_errors(fetched_page)
        elif status == 'not found':
            flagged_errors = [] # Will remove all the pages rows and add nothing new
        elif status == 'failed':
            logging.error(f'Failed to refresh page "{page_title}"')
            return # Just don't to anything in this case
        try:
            cur = self.conn.cursor()
            cur.execute(f'DELETE FROM no_target WHERE page_title = %s', (page_title,))
            rows = [
                (page_title, citeref, int(is_genuine_error), now_ts)
                for (citeref, is_genuine_error) in flagged_errors
            ]
            cur.executemany(f'INSERT INTO no_target(page_title, missing_anchor, genuine_error, last_updated) VALUES (%s, %s, %s, %s)', rows)
        finally:
            cur.close()
            self.conn.commit()

    def refresh_category(self, refresh_kind: Literal["soft", "hard"]):
        category_pages = get_pages_in_category()
        database_pages = get_pages_in_database(self.conn)

        need_deleting = database_pages - category_pages
        try:
            cur = self.conn.cursor()
            cur.executemany(f'DELETE FROM no_target WHERE page_title = %s', ((title,) for title in need_deleting))
        finally:
            cur.close()
            self.conn.commit()

        if refresh_kind == "soft":
            to_refresh = category_pages - database_pages
        elif refresh_kind == "hard":
            to_refresh = category_pages

        for page in to_refresh:
            self.refresh_page(page)

    def pages_with_issues(self) -> List[Tuple[str, int, int]]:
        try:
            cur = self.conn.cursor()
            cur.execute("""
            SELECT page_title, COUNT(missing_anchor), COUNT(DISTINCT missing_anchor)
            FROM no_target
            WHERE genuine_error = 1
            GROUP BY page_title
            ORDER BY COUNT(missing_anchor) DESC
            """)
            return [(page_title.decode('utf8'), count, distinct_count) for (page_title, count, distinct_count) in cur.fetchall()]
        finally:
            cur.close()

    def broken_citerefs_for_page(self, page_title: str) -> List[Tuple[str, int, int]]:
        try:
            cur = self.conn.cursor()
            cur.execute(f"""
            SELECT
                missing_anchor,
                COUNT(missing_anchor),
                (
                    SELECT COUNT(DISTINCT page_title) FROM citerefs
                    WHERE citeref_text = SUBSTRING(missing_anchor, 8)
                )
            FROM no_target
            WHERE genuine_error = 1
            AND page_title = %s
            GROUP BY missing_anchor
            ORDER BY COUNT(missing_anchor) DESC
            """, (page_title,))
            return list(cur.fetchall())
        finally:
            cur.close()