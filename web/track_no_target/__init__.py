from flask import Blueprint, render_template, redirect
from .. import db
from .backend import Backend

bp = Blueprint('track-no-target', __name__, url_prefix='/track-no-target')

@bp.route("/")
def main_page():
    conn = db.get_db()
    backend = Backend(conn)
    pages = backend.pages_with_issues()
    annotated_pages = [{
        'href': 'page/' + title,
        'text': title.replace('_', ' '),
        'count': count,
        'distinct_count': distinct_count
    } for (title, count, distinct_count) in pages]
    return render_template('no-target-main.html', pages=annotated_pages)

@bp.route('/page/<path:title>')
def subpage(title: str):
    conn = db.get_db()
    backend = Backend(conn)
    title_obj = {
        'slug': title,
        'text': title.replace('_', ' ')
    }
    citerefs = backend.broken_citerefs_for_page(title)
    annotated_citerefs = [{
        'citeref': citeref,
        'count': count,
        'elsewhere_count': elsewhere_count
    } for (citeref, count, elsewhere_count) in citerefs]
    return render_template('no-target-single-page.html', page=title_obj, citerefs=annotated_citerefs)

@bp.route('/refresh/<path:title>', methods=['POST'])
def refresh_page(title: str):
    conn = db.get_db()
    backend = Backend(conn)
    backend.refresh_page(title)
    return redirect('/track-no-target/page/' + title)