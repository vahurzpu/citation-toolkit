# This script is intended to be run from the command line

from backend import Backend
import toolforge

with toolforge.toolsdb('s55114__citation_toolkit') as conn:
    backend = Backend(conn)
    backend.refresh_category(refresh_kind='soft')